from keras.models import Model
from keras.optimizers import RMSprop
from keras import backend as K
from keras.layers import Input, Dense, Lambda, Dropout, Activation, LSTM, \
        TimeDistributed, Convolution1D, MaxPooling1D
from sklearn.model_selection import train_test_split
import numpy as np
import cPickle

SEED = 42
N_LAYERS = 3
EPOCH_COUNT = 100

def train_model(data):
    x = data['x']
    y = data['y']
    (x_train, x_val, y_train, y_val) = train_test_split(x, y, 
            test_size=0.3, random_state=SEED)
    print 'Building model...'

    n_features = x_train.shape[2]
    input_shape = (None, n_features)
    model_input = Input(input_shape, name='input')
    layer = model_input
    for i in range(N_LAYERS):
        # convolutional layer names might be needed to extract filters
        layer = Convolution1D(
                nb_filter=CONV_FILTER_COUNT,
                filter_length=FILTER_LENGTH,
 		name='convolution_' + str(i + 1)
            )(layer)
        layer = Activation('relu')(layer)
        layer = MaxPooling1D(2)(layer)
    
    layer = Dropout(0.5)(layer)
    layer = LSTM(LSTM_COUNT, return_sequences=True)(layer)
    layer = Dropout(0.5)(layer)
    # below line of code should be replaced
    # the layer should need way more neurons than the number of GENRES
    # we are trying to represent musical element in a spectrogram image 
    # that the CNN will extract
    # I'm thinking, a vector of size 50-100ish
    layer = TimeDistributed(Dense(len(GENRES)))(layer) 
    time_distributed_merge_layer = Lambda(
            function=lambda x: K.mean(x, axis=1),
            output_shape=lambda shape: (shape[0],) + shape[2:],
            name='output_merged'
        )
    model_output = time_distributed_merge_layer(layer)
    
    # insert code for LSTM that takes CNN output vectors as inputs
    # it should output a 50-100 dimension embedding for the audio
    # distance in this space should correspond to audio similarity
    # songs should output as nearest neighbours their cover versions.
    # hummed queries should output (after transfer learning) the 
    # intended songs.
    # it seems simple but i can't figure out a rule that the network 
    # can optimize (like how class label outputs can be optimized for by
    # categorical cross entropy). some representation of output/labels 
    # such that the network can make covers close neighbours of their 
    # originals.
    
    model = Model(model_input, model_output)
    opt = RMSProp(lr=0.00001)
    model.compile(
            loss='categorical_crossentropy',
            optimizer=opt,
            metrics=['accuracy']
        )

    print 'Training...'
    model.fit(x_train, y_train, batch_size=BATCH_SIZE, nb_epoch=EPOCH_COUNT, 
              validation_data(x_val, y_val), verbose=1)
    return model

if __name__=='__main__':
    parser = OptionParser()
    # get input data and some parameters

    
